/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include "nvs.h"
#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_log.h"
#include "esp_bt.h"
#include "esp_bt_main.h"
#include "esp_gap_bt_api.h"
#include "esp_bt_device.h"
#include "esp_spp_api.h"
#include "esp_int_wdt.h"
#include "esp_task_wdt.h"
//#include <rom/ets_sys.h>


#include "time.h"
#include "sys/time.h"
#include "string.h"
#include "driver/uart.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_intr_alloc.h"
#include "esp32/rom/uart.h"
#include "driver/gpio.h"

#define SPP_TAG 						"HighTest"
#define SPP_SERVER_NAME 				"HightTest_SPP_SERVER"
#define SPP_SHOW_DATA 					0
#define SPP_SHOW_SPEED 					1
#define SPP_SHOW_MODE SPP_SHOW_DATA    /*Choose show mode: show data or speed*/

static const esp_spp_mode_t 	esp_spp_mode = ESP_SPP_MODE_CB;
static const esp_spp_sec_t      sec_mask     = ESP_SPP_SEC_AUTHENTICATE;
static const esp_spp_role_t     role_slave   = ESP_SPP_ROLE_SLAVE;

/*DEFINES */
	/* UART PINS    */
		#define ECHO_TXD  		 (GPIO_NUM_17)
		#define ECHO_RXD  		 (GPIO_NUM_16)
		#define ECHO_TEST_RTS    (UART_PIN_NO_CHANGE)
		#define ECHO_TEST_CTS    (UART_PIN_NO_CHANGE)
    /*UART Configs*/
		#define ECHO_UARTNUM     UART_NUM_2
		#define ECHO_BAUDRATE    115200
		#define ECHO_DATABITS    UART_DATA_8_BITS
		#define ECHO_PARITY      UART_PARITY_DISABLE
		#define ECHO_STOP        UART_STOP_BITS_1
		#define ECHO_HW_FLOWCTRL UART_HW_FLOWCTRL_DISABLE
		#define ECHO_UARTCLK     UART_SCLK_APB
        #define ECHO_BUF_SIZE   (2048)
	/* LED PINS     */
		#define RUN_LED             GPIO_NUM_33
		#define CONNECT_LED         GPIO_NUM_32
		#define GPIO_OUTPUT_PIN_SEL  ((1ULL<<RUN_LED) | (1ULL<<CONNECT_LED))
    /* CHARS     */
		#define ESC         	0x1B
		#define ENTER       	0x0A
		#define CR         		0x0D
		#define LF          	0x0A
		#define ETX         	0x03
		#define STX         	0x02
		#define ACK         	0x06
	/* LOGS     */
	    #define LOG_TAG 		"HTestECHO"
		#define BUF_SIZE        (1024)
	/* COMMANDS     */
		//#define BTNAME_DEFAULT	"HighTestxxx"
#define BTNAME_DEFAULT	"ARES-2002312"
    	#define CMD_AT	 		"AT\r\n"
		#define CMD_INIT	 	"AT+INIT\r\n"
        #define CMD_NAME	 	"AT+BNAME="
        #define CMD_NAMEASK	 	"AT+BNAME=?\r\n"

        #define CMD_CONN	 	"CONNENCT\r\n"
		#define CMD_DISCONN	 	"DISCONNENCT\r\n"
		#define CMD_OK	 		"OK\r\n"
		#define CMD_ERR	 		"ERR\r\n"
        #define CMD_STOP	 	"STOP\r\n"

	/*BLE SSP  */
/*Variables*/
typedef enum {
	ST_ECHO_IDLE                           =0,
	ST_ECHO_WAIT_BL                        =1,
	ST_ECHO_CONNECT_BL                     =2,
	ST_ECHO_DISCONNECT_BL                  =3,
} ECHO_States;

typedef  struct uart_echo_s {
	char     					TXBuffer[BUF_SIZE];
	uint16_t 					TXBuffer_len;
	char     					RXBuffer[BUF_SIZE];
	uint16_t 					RXBuffer_len;
	ECHO_States     			EchoState;
	char     					BtName[BUF_SIZE];
	uint32_t                    handle;
} uart_echo_t;
uart_echo_t 					UartEcho;
bool connectstate=false;
bool disconnectstate=false;

/*************************************************************************/

static void esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param)
{
    switch (event) {
    case ESP_SPP_INIT_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_INIT_EVT");
        esp_bt_dev_set_device_name(UartEcho.BtName);
        esp_bt_gap_set_scan_mode(ESP_BT_CONNECTABLE, ESP_BT_GENERAL_DISCOVERABLE);
        esp_spp_start_srv(sec_mask,role_slave, 0, SPP_SERVER_NAME);
        break;
    case ESP_SPP_DISCOVERY_COMP_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_DISCOVERY_COMP_EVT");
        break;
    case ESP_SPP_OPEN_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_OPEN_EVT");
        break;
    case ESP_SPP_CLOSE_EVT:
    	if(UartEcho.EchoState==ST_ECHO_CONNECT_BL){
    		disconnectstate=true;
    		UartEcho.EchoState=ST_ECHO_DISCONNECT_BL;
    	}
        ESP_LOGI(SPP_TAG, "ESP_SPP_CLOSE_EVT");
        break;
    case ESP_SPP_START_EVT:
    	UartEcho.handle = param->cong.handle;
        ESP_LOGI(SPP_TAG, "ESP_SPP_START_EVT");
        ESP_LOGI(SPP_TAG, "handle=%d",param->data_ind.handle);
        break;
    case ESP_SPP_CL_INIT_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_CL_INIT_EVT");
        break;
    case ESP_SPP_DATA_IND_EVT:
    	if(UartEcho.EchoState==ST_ECHO_CONNECT_BL){
        	uart_write_bytes(ECHO_UARTNUM, (const char *) param->data_ind.data, param->data_ind.len);
        	UartEcho.handle = param->cong.handle;
        	esp_log_buffer_char("",param->data_ind.data,param->data_ind.len);
    	}
    	ESP_LOGI(SPP_TAG, "ESP_SPP_DATA_IND_EVT len=%d handle=%d",param->data_ind.len, param->data_ind.handle);
        break;
    case ESP_SPP_CONG_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_CONG_EVT");
        break;
    case ESP_SPP_WRITE_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_WRITE_EVT");
        break;
    case ESP_SPP_SRV_OPEN_EVT:
    	if(UartEcho.EchoState==ST_ECHO_WAIT_BL){
    		connectstate=true;
    	}
        ESP_LOGI(SPP_TAG, "ESP_SPP_SRV_OPEN_EVT");
        break;
    case ESP_SPP_SRV_STOP_EVT:
    	if(UartEcho.EchoState==ST_ECHO_CONNECT_BL){
    		disconnectstate=true;
    		UartEcho.EchoState=ST_ECHO_DISCONNECT_BL;
    	}
        ESP_LOGI(SPP_TAG, "ESP_SPP_SRV_STOP_EVT");
        break;
    case ESP_SPP_UNINIT_EVT:
        ESP_LOGI(SPP_TAG, "ESP_SPP_UNINIT_EVT");
        break;
    default:
        break;
    }
}
void esp_bt_gap_cb(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param)
{
    switch (event) {
    case ESP_BT_GAP_AUTH_CMPL_EVT:{
        if (param->auth_cmpl.stat == ESP_BT_STATUS_SUCCESS) {
            ESP_LOGI(SPP_TAG, "authentication success: %s", param->auth_cmpl.device_name);
            esp_log_buffer_hex(SPP_TAG, param->auth_cmpl.bda, ESP_BD_ADDR_LEN);
        } else {
            ESP_LOGE(SPP_TAG, "authentication failed, status:%d", param->auth_cmpl.stat);
        }
        break;
    }
    case ESP_BT_GAP_PIN_REQ_EVT:{
        ESP_LOGI(SPP_TAG, "ESP_BT_GAP_PIN_REQ_EVT min_16_digit:%d", param->pin_req.min_16_digit);
        if (param->pin_req.min_16_digit) {
            ESP_LOGI(SPP_TAG, "Input pin code: 0000 0000 0000 0000");
            esp_bt_pin_code_t pin_code = {0};
            esp_bt_gap_pin_reply(param->pin_req.bda, true, 16, pin_code);
        } else {
            ESP_LOGI(SPP_TAG, "Input pin code: 1234");
            esp_bt_pin_code_t pin_code;
            pin_code[0] = '1';
            pin_code[1] = '2';
            pin_code[2] = '3';
            pin_code[3] = '4';
            esp_bt_gap_pin_reply(param->pin_req.bda, true, 4, pin_code);
        }
        break;
    }
/*
#if (CONFIG_BT_SSP_ENABLED == true)
    case ESP_BT_GAP_CFM_REQ_EVT:
        ESP_LOGI(SPP_TAG, "ESP_BT_GAP_CFM_REQ_EVT Please compare the numeric value: %d", param->cfm_req.num_val);
        esp_bt_gap_ssp_confirm_reply(param->cfm_req.bda, true);
        break;
    case ESP_BT_GAP_KEY_NOTIF_EVT:
        ESP_LOGI(SPP_TAG, "ESP_BT_GAP_KEY_NOTIF_EVT passkey:%d", param->key_notif.passkey);
        break;
    case ESP_BT_GAP_KEY_REQ_EVT:
        ESP_LOGI(SPP_TAG, "ESP_BT_GAP_KEY_REQ_EVT Please enter passkey!");
        break;
#endif
*/
    default: {
        ESP_LOGI(SPP_TAG, "event: %d", event);
        break;
    }
    }
    return;
}
/**************************************************************/


static void InitUart(void){
    /* Configure parameters of an UART driver,
     * communication pins and install the driver
     */
    uart_config_t uart_config = {
    					.baud_rate = ECHO_BAUDRATE,
						.data_bits = ECHO_DATABITS,
						.parity    = ECHO_PARITY,
						.stop_bits = ECHO_STOP,
						.flow_ctrl = ECHO_HW_FLOWCTRL,
						.source_clk= ECHO_UARTCLK,
    };
    uart_driver_install(ECHO_UARTNUM, ECHO_BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_param_config  (ECHO_UARTNUM, &uart_config);
    uart_set_pin       (ECHO_UARTNUM, ECHO_TXD, ECHO_RXD, ECHO_TEST_RTS, ECHO_TEST_CTS);
    ESP_LOGI(LOG_TAG, "Init Uart");
}
static void InitEcho(void){
	UartEcho.EchoState     		= ST_ECHO_IDLE;
	UartEcho.TXBuffer_len     	= 0;
	UartEcho.RXBuffer_len     	= 0;
	snprintf(UartEcho.BtName,BUF_SIZE, "%s",BTNAME_DEFAULT);
	ESP_LOGI(LOG_TAG, "Init Echo");
}
bool isCRLF(void){
    if (UartEcho.RXBuffer_len>=2){
        if (UartEcho.RXBuffer[UartEcho.RXBuffer_len-2]==CR && UartEcho.RXBuffer[UartEcho.RXBuffer_len-1]==LF ){
            return true;
        }
    }
    return false;
}
bool DeInitBluetooth(void){
	esp_bt_controller_deinit();
	esp_bt_controller_disable();
	esp_bluedroid_disable();
	esp_spp_deinit();
	ESP_LOGI("EchoTask", "deinit bt");
    return true;
}
bool InitBluetooth(void){
	esp_err_t ret;
	ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_BLE));
	esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
	if ((ret = esp_bt_controller_init(&bt_cfg)) != ESP_OK) {
	    ESP_LOGE(SPP_TAG, "%s initialize controller failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
	}
//	ESP_LOGI("EchoTask", "1");
    if ((ret = esp_bt_controller_enable(ESP_BT_MODE_CLASSIC_BT)) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s enable controller failed: %s\n", __func__, esp_err_to_name(ret));
        return false;
    }
//	ESP_LOGI("EchoTask", "2");
    if ((ret = esp_bluedroid_init()) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s initialize bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
    }
//	ESP_LOGI("EchoTask", "3");
    if ((ret = esp_bluedroid_enable()) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s enable bluedroid failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
    }
//	ESP_LOGI("EchoTask", "4");
    if ((ret = esp_bt_gap_register_callback(esp_bt_gap_cb)) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s gap register failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
    }
//	ESP_LOGI("EchoTask", "5");
    if ((ret = esp_spp_register_callback(esp_spp_cb)) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s spp register failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
    }
//	ESP_LOGI("EchoTask", "6");
    if ((ret = esp_spp_init(esp_spp_mode)) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s spp init failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
    }
//	ESP_LOGI("EchoTask", "7");
    esp_bt_pin_type_t pin_type = ESP_BT_PIN_TYPE_VARIABLE;
    esp_bt_pin_code_t pin_code;
    if ((ret = esp_bt_gap_set_pin(pin_type, 0, pin_code)) != ESP_OK) {
        ESP_LOGE(SPP_TAG, "%s spp init failed: %s\n", __func__, esp_err_to_name(ret));
	    return false;
    }
//	ESP_LOGI("EchoTask", "8");
    return true;
}
bool CommandDetectStop(void){
	if(strncmp(UartEcho.RXBuffer,CMD_STOP,strlen(UartEcho.RXBuffer)) == 0 ){
		UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_OK);
		UartEcho.EchoState = ST_ECHO_IDLE;
		DeInitBluetooth();
	}else{
		UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_ERR);
	}
	uart_write_bytes(ECHO_UARTNUM, (const char *) UartEcho.TXBuffer, UartEcho.TXBuffer_len);
	return true;
}
bool CommandDetect(void){
	if(strncmp(UartEcho.RXBuffer,CMD_AT,strlen(UartEcho.RXBuffer)) == 0 ){
		UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_OK);
	}else if(strncmp(UartEcho.RXBuffer,CMD_INIT,strlen(UartEcho.RXBuffer)) == 0 ){
		if(InitBluetooth()){
			UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_OK);
		}else{
			UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_ERR);
		}
		UartEcho.EchoState = ST_ECHO_WAIT_BL;
	}else if(strncmp(UartEcho.RXBuffer,CMD_NAMEASK,strlen(CMD_NAMEASK)) == 0 ){
		UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s%s\r\n",CMD_NAME,UartEcho.BtName);
	}else if(strncmp(UartEcho.RXBuffer,CMD_NAME,strlen(CMD_NAME)) == 0 ){
		if(UartEcho.RXBuffer_len  > strlen(CMD_NAME)+2){
			memset(UartEcho.BtName,0,strlen(UartEcho.BtName));
			memcpy (UartEcho.BtName,&UartEcho.RXBuffer[strlen(CMD_NAME)],UartEcho.RXBuffer_len-strlen(CMD_NAME)-2);
			UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_OK);
		}else{
			UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_ERR);
		}
	}else{
		UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_ERR);
	}
	uart_write_bytes(ECHO_UARTNUM, (const char *) UartEcho.TXBuffer, UartEcho.TXBuffer_len);
	ESP_LOGI("EchoTask", "TXBuf     = %s",UartEcho.TXBuffer);
	return true;
}
void EchoTask(void *arg){
	while (1) {
		if(UartEcho.RXBuffer_len==0){
			UartEcho.RXBuffer_len = uart_read_bytes(ECHO_UARTNUM, (uint8_t *)UartEcho.RXBuffer, BUF_SIZE, 20 / portTICK_RATE_MS);
/*			if(UartEcho.RXBuffer_len!=0){
				ESP_LOGI("EchoTask", "RxBuf Len !0 = %d",UartEcho.RXBuffer_len);
			}*/
		}
	    switch (UartEcho.EchoState) {
			case ST_ECHO_IDLE:
		    	if(isCRLF()){
//		    		strcpy(UartEcho.TXBuffer, UartEcho.RXBuffer);
//		    		UartEcho.TXBuffer_len=UartEcho.RXBuffer_len;
//		    		UartEcho.RXBuffer_len=0;
//                  uart_write_bytes(ECHO_UARTNUM, (const char *) UartEcho.TXBuffer, UartEcho.TXBuffer_len);
//		    		ESP_LOGI("EchoTask", "RxBuf Len = %d",UartEcho.RXBuffer_len);
		    		ESP_LOGI("EchoTask", "RxBuf     = %s",UartEcho.RXBuffer);
		    		CommandDetect();
		    		memset(UartEcho.RXBuffer,0,UartEcho.RXBuffer_len);
		    		UartEcho.RXBuffer_len=0;
		    	}else{
		    		UartEcho.RXBuffer_len=0;
		    	}
				break;
			case ST_ECHO_WAIT_BL:
				if(connectstate){
					UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_CONN);
					uart_write_bytes(ECHO_UARTNUM, (const char *) UartEcho.TXBuffer, UartEcho.TXBuffer_len);
					ESP_LOGI("EchoTask", "TXBuf     = %s",UartEcho.TXBuffer);
					connectstate=false;
					UartEcho.EchoState=ST_ECHO_CONNECT_BL;
					//gpio_set_level(CONNECT_LED,1);
				}else{
			    	if(isCRLF()){
			    		CommandDetectStop();
			    		memset(UartEcho.RXBuffer,0,UartEcho.RXBuffer_len);
			    		UartEcho.RXBuffer_len=0;
			    	}else{
			    		memset(UartEcho.RXBuffer,0,UartEcho.RXBuffer_len);
			    		UartEcho.RXBuffer_len=0;
			    	}
//					if (UartEcho.RXBuffer_len>=1){
//						memset(UartEcho.RXBuffer,0,UartEcho.RXBuffer_len);
//						UartEcho.RXBuffer_len=0;
//					}
				}
				break;
			case ST_ECHO_CONNECT_BL:
				 if (UartEcho.RXBuffer_len>=1){
					 ESP_LOGI("EchoTask", "RxBuf     = %s",UartEcho.RXBuffer);
					 esp_spp_write(UartEcho.handle, UartEcho.RXBuffer_len, (uint8_t *)UartEcho.RXBuffer);
					 UartEcho.RXBuffer_len=0;
				 }
				break;
			case ST_ECHO_DISCONNECT_BL:
				if(disconnectstate){
					UartEcho.TXBuffer_len = snprintf(UartEcho.TXBuffer,BUF_SIZE, "%s",CMD_DISCONN);
					uart_write_bytes(ECHO_UARTNUM, (const char *) UartEcho.TXBuffer, UartEcho.TXBuffer_len);
					ESP_LOGI("EchoTask", "TXBuf     = %s",UartEcho.TXBuffer);
					disconnectstate=false;
					UartEcho.EchoState=ST_ECHO_WAIT_BL;
					//gpio_set_level(CONNECT_LED,0);
				}else{
					if (UartEcho.RXBuffer_len>=1){
						memset(UartEcho.RXBuffer,0,UartEcho.RXBuffer_len);
						UartEcho.RXBuffer_len=0;
					}
				}
				break;
			default:
				break;
			}
		}
}
void InitLeds(void){
    gpio_config_t io_conf;
    io_conf.intr_type 		= GPIO_PIN_INTR_DISABLE;
    io_conf.mode 			= GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask 	= GPIO_OUTPUT_PIN_SEL;
    io_conf.pull_down_en 	= 0;
    io_conf.pull_up_en 		= 0;
    gpio_config(&io_conf);
    gpio_set_level(RUN_LED,1);
    gpio_set_level(CONNECT_LED,0);
}

void app_main(void){
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );
    InitUart();
    InitEcho();
    esp_task_wdt_deinit();
    esp_task_wdt_status(NULL);
    //InitLeds();
    xTaskCreate(EchoTask, "EchoTask", 1024*2, NULL, 5, NULL);
    return;
}
